FROM hub.ts.co.ir/alpine:3.7

LABEL maintainer="NGINX Docker Maintainers <docker-maint@nginx.com>"

ENV NGINX_VERSION 1.15.0

RUN addgroup -S nginx \
	&& adduser -D -S -h /var/cache/nginx -s /sbin/nologin -G nginx nginxs
EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["sh"]

